//
//  NiceListCollectionViewCell.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/15/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//

import UIKit
import CoreData

protocol NiceListCollectionViewCellDelegate {
    func niceListCollectionViewCellDidRemoveNice(cell : NiceListCollectionViewCell)
}

class NiceListCollectionViewCell: UICollectionViewCell {
    
    var delegate : NiceListCollectionViewCellDelegate?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
     
    
    override func drawRect(rect: CGRect) {
        self.layer.masksToBounds = false;
        self.layer.shadowOffset = CGSizeMake(4, 4);
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        self.layer.shadowColor = UIColor.lightGrayColor().CGColor
    }
    
    func loadItem(product:NSManagedObject){
        if let strUrl = product.valueForKey("imageURL") as? String{
            imageView.sd_setImageWithURL(NSURL(string: strUrl ))
        }
        
        self.priceLabel.text = product.valueForKey("price") as? String
        self.titleLabel.text = product.valueForKey("title") as? String
    }
    
    @IBAction func removeButtonTouched(sender: UIButton) {
        self.delegate?.niceListCollectionViewCellDidRemoveNice(self)
    }

}
