//
//  ProductResultsCollectionViewController.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/15/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "ProductResultsCollectionViewCell"

class ProductResultsCollectionViewController: UICollectionViewController, NSXMLParserDelegate, ProductResultsCollectionViewCellDelegate {
    
    var searchValue : String?
    var products : Array<Product> = Array<Product>()
    
    var niceProducts = [NSManagedObject]()
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = searchValue
         loadNiceListFromCoreData()
        if let searchValue = self.searchValue,
            manager = RWMAmazonProductAdvertisingManager(accessKeyID: aws_accessKeyID, secret: aws_secretKey){
            
            let hud =  MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            hud.labelText = "Loading"
            hud.color = UIColor.grayColor()
            manager.itemSearchOperation(withKeyword: searchValue, success: { (responseObject) in
                let data = responseObject as! NSData
                let xmlParser = NSXMLParser(data: data)
                xmlParser.delegate = self
                xmlParser.parse()
            }) { (error) in
                
            }
        }
        self.collectionView!.registerNib(UINib(nibName: "ProductResultsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
 
    }
 
 

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return  1;
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return products.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ProductResultsCollectionViewCell
        
        if isProductAtIndexPathNice(indexPath) {
            cell.loadItem(products[indexPath.row], selected:true)
        }
        else{
            cell.loadItem(products[indexPath.row], selected:false)
        }
        cell.delegate = self
     
    
        return cell
    }
    
    
    //MARK: XML Parser Delegate Methods
    
    var element = ""
    var pAsin = ""
    var pTitle  = ""
    var pPrice = ""
    var pStrImgURL = ""
    
    var inImageSet = false
    var inLargeImage = false
    var inListPrice = false
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        element = elementName
        if(elementName == "Item")
        {
            pAsin = ""
            pTitle  = ""
            pPrice = ""
            pStrImgURL = ""
        }
        else if (elementName == "ImageSets"){
            inImageSet =  true
        }
        else if (elementName == "ListPrice"){
            inListPrice = true
        }else if (elementName == "LargeImage"){
            inLargeImage = true
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        
        if (element == "Title") {
            pTitle = string
        }else if element == "ASIN"{
            pAsin = string
        }else if (element == "FormattedPrice" && inListPrice){
            pPrice = string
        }else if (element == "URL" && !inImageSet && inLargeImage){
            pStrImgURL = string
        }
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if(elementName == "Item")
        {
            products.append(Product(asin: pAsin, title: pTitle, price: pPrice, strImgURL: pStrImgURL))
        }
        else if (elementName == "ImageSets"){
            inImageSet =  false
        }
        else if (elementName == "ListPrice"){
            inListPrice = false
        }
        else if (elementName == "LargeImage"){
            inLargeImage = false
        }
    }
    
    func parserDidEndDocument(parser: NSXMLParser) {
        dispatch_async(dispatch_get_main_queue()) { 
            MBProgressHUD.hideHUDForView(self.view, animated: true)
        } 
        self.collectionView?.reloadData() 
    }
    
    //MARK: ProductResultsCollectionViewCellDelegate Methods
    
    func productResultsCollectionViewCellDidTouchNice(cell: ProductResultsCollectionViewCell) {
        if let indexPath = self.collectionView?.indexPathForCell(cell){
            
            let appDelegate =  UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let entity =  NSEntityDescription.entityForName("Product", inManagedObjectContext:managedContext)
            let product = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            
            product.setValue(products[indexPath.row].asin, forKey: "asin")
            product.setValue(products[indexPath.row].title, forKey: "title")
            product.setValue(products[indexPath.row].strImgURL, forKey: "imageURL")
            product.setValue(products[indexPath.row].price, forKey: "price")
            do {
                try managedContext.save()
                niceProducts.append(product)
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
            
        }
    }
    
    func isProductAtIndexPathNice(indexPath:NSIndexPath) -> Bool{
        for niceProduct in niceProducts {
            if niceProduct.valueForKey("asin") as? String == products[indexPath.row].asin{
                return true
            }
        }
        return false
    }
    
    
    //MARK: Core Data Methods
    
    func loadNiceListFromCoreData(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Product")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            niceProducts = results as! [NSManagedObject]
            
            self.collectionView?.reloadData()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func deleteNiceProductIfLocatedAtIndexPath(indexPath:NSIndexPath) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        for niceProduct in niceProducts {
            if niceProduct.valueForKey("asin") as? String == products[indexPath.row].asin{
                managedContext.deleteObject(niceProduct)
                break
            }
        }
    }
    
    func productResultsCollectionViewCellDidRemoveNice(cell: ProductResultsCollectionViewCell) {
        if let indexPath = self.collectionView?.indexPathForCell(cell){
            deleteNiceProductIfLocatedAtIndexPath(indexPath)
        }
    }
    
}
