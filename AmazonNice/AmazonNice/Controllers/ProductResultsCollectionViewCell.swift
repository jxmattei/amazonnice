//
//  ProductResultsCollectionViewCell.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/15/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//

import UIKit

protocol ProductResultsCollectionViewCellDelegate {
    func productResultsCollectionViewCellDidTouchNice(cell : ProductResultsCollectionViewCell)
    func productResultsCollectionViewCellDidRemoveNice(cell : ProductResultsCollectionViewCell)
}

class ProductResultsCollectionViewCell: UICollectionViewCell {

    var product : Product?
    var delegate : ProductResultsCollectionViewCellDelegate?
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var niceButton: UIButton!
     
    
    override func drawRect(rect: CGRect) {
        self.layer.masksToBounds = false;
        self.layer.shadowOffset = CGSizeMake(4, 4);
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        self.layer.shadowColor = UIColor.lightGrayColor().CGColor
    }
    
    func loadItem(product:Product, selected : Bool){
        self.product = product
        imageView.sd_setImageWithURL(NSURL(string:  product.strImgURL))
            self.priceLabel.text = product.price
            self.titleLabel.text = product.title
            if (selected){
                self.niceButton.selected = true
            }
            else{
                self.niceButton.selected = false
            }
        self.product = product
    }

    @IBAction func niceButtonTouched(sender: UIButton) {
        if (sender.selected == true){
            self.delegate?.productResultsCollectionViewCellDidRemoveNice(self)
            sender.selected  = false
        }
        else{
            self.delegate?.productResultsCollectionViewCellDidTouchNice(self)
            sender.selected =  true
        }
    }
}
