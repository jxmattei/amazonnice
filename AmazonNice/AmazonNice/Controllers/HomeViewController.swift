//
//  HomeViewController.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/14/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//

import UIKit
import CoreData

class HomeViewController: UIViewController, NSXMLParserDelegate,UITextFieldDelegate{
    
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var viewForm: UIView!
     
    
    @IBOutlet weak var topFormViewLayoutConstraint: NSLayoutConstraint!
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) { 
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.topFormViewLayoutConstraint.constant = 150
        self.viewForm.alpha = 0 
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        
        goToCenterAnimation()
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        super.viewWillDisappear(animated)
    }
    
    
    func goToCenterAnimation(){
        UIView .animateWithDuration(0.7) {
            self.topFormViewLayoutConstraint.constant = 240
            self.viewForm.alpha = 1
            self.viewForm.layoutIfNeeded()
        }
    }
    

    
    //MARK: Actions
    
    @IBAction func searchValue(sender:UIButton?)
    {
        if txtSearch.text!.isSearchValid() {
            let resultsViewController = ProductResultsCollectionViewController(nibName: "ProductResultsCollectionViewController", bundle: nil)
            resultsViewController.searchValue = txtSearch.text!
            
            self.navigationController?.pushViewController(resultsViewController, animated: false) 
        }
        else {
            let alert = UIAlertController(title: "Empty Search", message: "Nothing to search", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
 
    
    @IBAction func goToNiceList(sender:UIButton){
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Product")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
            if results.count > 0 {
                let niceListViewController = NiceListCollectionViewController(nibName: "NiceListCollectionViewController", bundle: nil)
                self.navigationController?.pushViewController(niceListViewController, animated: true)
                
            }
            else{
                let alert = UIAlertController(title: "No Nice Products", message: "No products have been assigned as Nice", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

        
    }
    
    
    //MARK: TextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if((textField.text?.isSearchValid()) == nil){
            searchValue(nil)
        }
        return false
    }
    
    
    
}
