//
//  NiceListCollectionViewController.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/15/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//

import UIKit
import CoreData
private let reuseIdentifier = "NiceListCollectionViewCell"

class NiceListCollectionViewController: UICollectionViewController, NiceListCollectionViewCellDelegate {
    
    var products =  [NSManagedObject]()
    
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let searchValue = "Nice Products"
        self.navigationItem.title = searchValue
        self.collectionView!.registerNib(UINib(nibName: "NiceListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.loadNiceListFromCoreData()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.collectionView?.reloadData()
    }
     
    // MARK: UICollectionViewDataSource
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return  1;
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! NiceListCollectionViewCell
        cell.loadItem(products[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    //MARK: Core Data Methods
    
    func loadNiceListFromCoreData(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Product")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            products = results as! [NSManagedObject]
            
            self.collectionView?.reloadData()
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
     
    //MARK: NiceListCollectionViewCellDelegate Methods
    func niceListCollectionViewCellDidRemoveNice(cell: NiceListCollectionViewCell) {
        if let indexPath = self.collectionView?.indexPathForCell(cell){
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            do{
                managedContext.deleteObject(self.products[indexPath.row])
                try managedContext.save()
                self.products.removeAtIndex(indexPath.row)
                self.collectionView?.performBatchUpdates({  self.collectionView?.deleteItemsAtIndexPaths([indexPath])
                    }, completion: { (finished) in
                        
                })
            }
            catch let error as NSError {
                print("Could not re \(error), \(error.userInfo)")
            }
            
            
        }
    }
    
    
}
