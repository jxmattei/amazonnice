//
//  Product.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/15/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//

import Foundation

struct Product{
    
    var asin : String
    var title : String
    var price : String
    var strImgURL : String    
    
}
