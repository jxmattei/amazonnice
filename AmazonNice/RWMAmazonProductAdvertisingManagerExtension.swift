//
//  RWMAmazonProductAdvertisingManagerExtension.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/15/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//

extension RWMAmazonProductAdvertisingManager{
    
    func itemSearchOperation(withKeyword keyword:String,  success:(responseObject:AnyObject!)-> Void, failure:(error:NSError) -> Void){
        self.responseSerializer = AFHTTPResponseSerializer()
        let parameters = [
            "SearchIndex" : "All",
            "Service" : "AWSECommerceService",
            "Operation" : "ItemSearch",
            "Keywords" : keyword,
            "ResponseGroup" : "ItemAttributes,Images",
            "AssociateTag" : "amazonnice12-20"
            
        ]
        
        self.enqueueRequestOperationWithMethod("GET", parameters: parameters, success: { (responseObject:AnyObject!) in
            success(responseObject: responseObject) 
        }) { (error : NSError!) in
            failure(error: error)
        }

        
    }
}


