//
//  StringExtension.swift
//  AmazonNice
//
//  Created by Jorge Xavier Mattei Perez on 4/14/16.
//  Copyright © 2016 Jorge Xavier Mattei Perez. All rights reserved.
//


extension String {
    func isSearchValid() -> Bool{
        if self.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
            ).characters.count > 0 {
            return true
        }
        return false
    }
}